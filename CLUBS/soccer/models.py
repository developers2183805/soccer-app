from django.db import models
class football(models.Model):
    club_name = models.CharField(max_length = 25)
    club_position = models.IntegerField(null = True)
    champ_trophies = models.IntegerField(null = True)



    def __str__(self):
      return f'{self.club_name} :   {self.champ_trophies} champ trophies  :   {self.club_position}th position'       

# Create your models here.
