from django.contrib import admin
from . models import football

class FootballAdmin(admin.ModelAdmin):
    list_display = ('club_name', 'champ_trophies', 'club_position')

admin.site.register(football, FootballAdmin)    

# Register your models here.
