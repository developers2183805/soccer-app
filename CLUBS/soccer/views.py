from django.http import HttpResponse
from django.template import loader
from .models import football
from django.contrib.auth.models import User


def futa(request):
    myclubs = football.objects.all().values()
    template = loader.get_template('all_clubs.html')
    context = {
        'myclubs' : myclubs,
    }
    return HttpResponse(template.render(context, request))

def details(request, id):
    myclub = football.objects.get(id = id)
    template = loader.get_template("details.html")
    context = {
        'myclub': myclub
    }
    return HttpResponse(template.render(context, request)) 

def main(request):
    template = loader.get_template('main.html')
    return HttpResponse ( template.render())

def testing(request):
    if request.method == 'POST':
        email = request.POST['Your Email']
        password = request.POST['Your Password']

        myuser = User.objects.create_user(email,password)
        myuser.save()
    
    template = loader.get_template('template.html')
  
    return  HttpResponse(template.render())


# Create your views here.
