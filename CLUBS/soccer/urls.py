from django.urls import path
from . import  views

urlpatterns = [
    path('main/soccer/', views.futa, name = 'soccer'),
    path('soccer/details/<int:id>', views.details, name = 'details'),
    
    path('testing/', views.testing, name = 'testing'),
    path('', views.main, name = 'main'),
    path('soccer/', views.futa, name = 'soccer')
]